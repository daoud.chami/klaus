
import os
import Levenshtein

EDIT_TRESHOLD = 0.2
LEN_THRESHOLD = 100

def readPdf(path):
	os.system("pdftotext "+path+" ./tmp.txt")
	content = list(filter(lambda x : len(x)>LEN_THRESHOLD, map(lambda x : x.strip(), open("./tmp.txt", "r", encoding="latin-1").readlines())))
	os.system("rm ./tmp.txt")
	return content

def distance(a, b):
	return Levenshtein.distance(a, b)/(len(a)*0.5+len(b)*0.5)

def compare(pathSrc, pathDst):
	#This function will be implemented in a purely O(n^2) way. No optimizations, just loops
	srcContent, dstContent = readPdf(pathSrc), readPdf(pathDst)

	editedDst = list(filter(lambda x : len(list(filter(lambda x : x>0 and x<EDIT_TRESHOLD,[distance(x, i) for i in srcContent])))>0, dstContent)) #distance<EDIT_THRESHOLD between src and edited
	editedSrc = list(filter(lambda x : len(list(filter(lambda x : x>0 and x<EDIT_TRESHOLD,[distance(x, i) for i in dstContent])))>0, srcContent)) #distance<EDIT_THRESHOLD between src and edited
	preserved = list(set(srcContent).intersection(set(dstContent))) # in src and  in dst
	new = list((set(dstContent).difference(srcContent)).difference(editedDst+editedSrc)) # not in src and in dst
	deleted = list((set(srcContent).difference(dstContent)).difference(editedDst+editedSrc)) # in src and not in dst
	return {"editedSrc":editedSrc, "editedDst":editedDst, "preserved":preserved, "new":new, "deleted":deleted}