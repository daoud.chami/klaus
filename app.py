#!/usr/bin/env python
"""
File name: app.py

Date created: 2018/09/18

Python Version: 3.6
"""

from flask import Flask, request, jsonify, render_template
import os
from werkzeug.utils import secure_filename
from utils import utils

UPLOAD_FOLDER = './data/uploads/'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

@app.route('/')
def root():
	return render_template("./index.html")
    


@app.route('/compare', methods=['POST'])
def predict():
	srcFile = request.files['srcFile']
	dstFile = request.files['dstFile']
	srcFile.save(UPLOAD_FOLDER+srcFile.name)
	dstFile.save(UPLOAD_FOLDER+dstFile.name)
	return jsonify(utils.compare(UPLOAD_FOLDER+srcFile.name,UPLOAD_FOLDER+dstFile.name)),200








if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000, debug=True)

# gunicorn --bind 0.0.0.0:5000 wsgi
